# -*- coding: utf-8 -*-
from PIL import Image
import numpy
import sys
import codecs
import datetime
import os

from PoreAlgorithm import flaternPic
from PoreAlgorithm import selectPores
from PoreAlgorithm import poreSizes
from PoreAlgorithm import averageDict
from PoreAlgorithm import blur
from PoreAlgorithm import cut
#from PoreAlgorithm import poreBoundaries

preprocessors = dict((
    ('cut', (4, cut)),
    ('blur', (1, blur)),
    ('select', (1, selectPores))
))

help = '''
must have at least 1 argument:
'<filename>' or '<help>'
image from file 'filename' would be processed
or help will be shown.

'cut <left> <top> <right> <bottom>' cuts borders of image.
'blur <delta>' blurs pixels in radius of <delta>.
'select <percent>' concludes preprocessing, making darkest
<percent> black and other part white.
'''

def openImage(fileName):
    try:
        im = Image.open(fileName)
        im.show()
    except:
        print('Not valid filename')
    else:
        return im

if __name__ == '__main__':
    if (sys.argv[1] == 'help'):
        print(help)
        exit(0)
    if len(sys.argv) <= 2:
        print('Starting GUI')
    else:
        filename = "_".join(sys.argv[1:])
        image = openImage(sys.argv[1])
        imageMatrix = numpy.array(image)
        processingArguments = sys.argv[2:]
        while len(processingArguments):
            function = preprocessors[processingArguments[0]][1]
            amount = preprocessors[processingArguments[0]][0]
            processingArguments = processingArguments[1:]
            imageMatrix = function(imageMatrix, processingArguments[:amount])
            processingArguments = processingArguments[amount:]

        poreSizesDict = poreSizes(imageMatrix)
        #boundaryRectDict = poreBoundaries(flatImageMatrix)
        averageSize = averageDict(poreSizesDict)

        result = Image.fromarray(imageMatrix.astype('uint8'))
        time = str(datetime.datetime.now().time())
        result.save('testing/' + filename + '_' + time + '.bmp')
        resultFile = open('testing/' + filename + '_' + time + '.result', 'w')
        for key, elem in sorted(poreSizesDict.items()):
            resultFile.write(str(key) + '\t\t' + str(elem) + '\n')
        resultFile.write(str(averageSize))
        resultFile.close()